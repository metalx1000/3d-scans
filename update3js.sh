#!/bin/bash
###################################################################### 
#Copyright (C) 2019  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

function main(){
  echo "Welcome..."
  echo "Cleaning up..."
  rm libs/*

  echo "Retrieving ThreeJS..."
  wget "https://raw.githubusercontent.com/mrdoob/three.js/dev/build/three.min.js" -O libs/three.js

  echo "Retrieving ColladaLoader..."
  wget "https://threejs.org/examples/js/loaders/ColladaLoader.js" -O libs/ColladaLoader.js

  echo "Retrieving Tween..."
  wget "https://raw.githubusercontent.com/tweenjs/tween.js/master/src/Tween.js" -O libs/Tween.js

  wget "https://threejs.org/examples/js/controls/OrbitControls.js" -O libs/OrbitControls.js
  exit 0
}

main

